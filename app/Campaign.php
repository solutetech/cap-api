<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'campaigns';
    
    protected $fillable = ['client_id','campaign_name','campaign_description','campaign_start_date','campaign_expiration_date'];
    
    // protected $hidden = ['id','created_at','updated_at'];
    
    public function client() {
        return $this->belongsTo('App\Client');
    }
    
    public function coupon() {
        return $this->hasMany('App\Coupon');
    }
}
