<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'cards';
    
    protected $fillable = ['client_id', 'card_type', 'card_number', 'card_expiration', 'security_code', 'name_on_card'];
    
    protected $hidden = ['id','client_id','card_type','card_number','card_expiration','security_code','name_on_card','created_at','updated_at'];
    
    public function client() {
        return $this->belongsTo('App\Client');
    }
}
