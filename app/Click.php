<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Click extends Model
{
    protected $table = 'clicks';
    
    protected $fillable = ['coupon_id','customer_id'];
    
    // protected $hidden = ['created_at','updated_at'];
    
    public function coupon() {
        return $this->belongsTo('App\Coupon');
    }
    
    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
