<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    
    protected $fillable = ['client_name','client_address','client_city','client_state','client_zip','client_phone','client_email','client_password','client_logo','first_name','last_name'];
    
    protected $hidden = ['client_password'];
    
    public function campaign() {
        return $this->hasMany('App\Campaign');
    }
    
    public function coupon() {
        return $this->hasMany('App\Coupon');
    }
    
    public function following() {
        return $this->hasMany('App\Following');
    }
}