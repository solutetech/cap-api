<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';
    
    protected $fillable = ['campaign_id','client_id','coupon_name','coupon_description','coupon_image','coupon_barcode','coupon_expiration','used_count'];
    
    protected $hidden = ['created_at','updated_at'];
    
    public function campaign() {
        return $this->belongsTo('App\Campaign');
    }
    
    public function client() {
        return $this->belongsTo('App\Client');
    }
    
    public function click() {
        return $this->hasMany('App\Click');
    }
    
    public function coupon_use() {
        return $this->hasMany('App\Coupon_Use');
    }
}