<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    
    protected $fillable = ['first_name','last_name','email','password','photo','points'];
    
    protected $hidden = ['password','created_at','updated_at'];
    
    public function following() {
        return $this->hasMany('App\Following');
    }
    
    public function used() {
        return $this->hasMany('App\Used');
    }
    
    public function click() {
        return $this->belongsTo('App\Click');
    }
}
