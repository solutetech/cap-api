<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Following extends Model
{
    protected $table = 'following';
    
    protected $fillable = ['customer_id','client_id'];
    
    protected $hidden = ['id','created_at','updated_at'];
    
    public function client() {
        return $this->belongsTo('App\Client');
    }
    
    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
