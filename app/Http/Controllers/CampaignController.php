<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Campaign;
use App\Coupon;

class CampaignController extends Controller
{
    public function index() {
        $campaigns = Campaign::all();
        return response()->json(['count' => $campaigns->count(), 'data' => $campaigns], 200);        
    }
    
    public function show($id) {
        $campaign = Campaign::find($id);
        return response()->json(['count' => $campaign->coupon->count(), 'data' => $campaign]);
    }
    
    public function create() {
        
    }
    
    public function update() {
        
    }
    
    public function destroy() {
        
    }
}
