<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Card;

class CardController extends Controller
{
    public function index() {
        $card = Card::all();
        return response()->json(['count'=>$card->count(), 'data' => $card], 200);
    }
    
    public function show() {
        
    }
    
    public function create() {
        
    }
    
    public function update() {
        
    }
    
    public function destroy() {
        
    }
}
