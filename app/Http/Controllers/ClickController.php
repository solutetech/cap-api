<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Click;

use App\Http\Requests\CreateClickRequest;

class ClickController extends Controller
{
    public function index() {
        $click = Click::all();
        return response()->json(['count'=>$click->count(), 'data' => $click], 200);
    }
    
    public function show($id) {
        $click = Click::find($id);
        return response()->json($click);
    }
    
    public function store(CreateClickRequest $request) {
        $values = $request->all();
        Click::create($values);
        
        return response()->json(['message' => 'Click successfully created', 'code' => 422], 422);
    }
    
    public function update() {
        
    }
    
    public function destroy() {
        
    }
}
