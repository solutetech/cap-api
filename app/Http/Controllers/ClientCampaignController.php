<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Client;
use App\Campaign;
use App\Coupon;

class ClientCampaignController extends Controller
{
    public function index($id) {
        $client = Client::find($id);
        
        if($client) {
            $status = 'success';
            $message = 'Client found!';
            $data['client'] = $client; //array('id' => $client->id, 'name' => $client->client_name, 'logo' => $client->client_logo);
            
            $coupons = Coupon::where('client_id', $client->id)->get();

            $data['coupons'] = $coupons;
            $data['coupon_count'] = $coupons->count();
            // $campaigns = Campaign::where('client_id', $client->id)->get();
            // $data['client']['campaign_count'] = count($campaigns);
            
            // for ($i = 0; $i < count($campaigns); $i++) {
            //     $data['client']['campaign'][$i] = $campaigns[$i];
            //     $data['client']['campaign'][$i]['coupon_count'] = $campaigns[$i]->coupon->count();
            //     $data['client']['coupon'][] = $campaigns[$i]->coupon;
            // }
        } else {
            $status = 'failed';
            $message = 'Client does not exist!';
            $data = "";
        }
        
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], 200);
    }
}
