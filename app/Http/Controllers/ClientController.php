<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Client;
use DB;

class ClientController extends Controller
{
    // Returns all clients
    public function index() {
        // $clients = DB::table("clients")
        //     ->select("clients.*",
        //         DB::raw('('. DB::select('SELECT COUNT(id) FROM coupons WHERE client_id = `clients`.`id`') . ') as coupon_count '))
        //     ->get();
        $clients = \DB::table("clients")
            ->select("clients.*",
                    \DB::raw("(SELECT COUNT(id) FROM coupons
                            WHERE coupons.client_id = clients.id
                        ) as coupon_count"))
            ->get();
        return response()->json(['data' => $clients], 200);
    }
    
    // Returns a single client
    public function show($id) {
        $client = Client::find($id);
        
        if (!$client) {
            return response()->json(['message' => 'Client does not exist.', 'code' => 404], 404);
        }
        
        return response()->json(['count' => $client->campaign->count(), 'data' => $client], 200);
    }
    
    // Creates a new client
    public function create() {
        
    }
    
    // Updates an existing client
    public function update() {
        
    }
    
    // Deletes a client
    public function destroy() {
        
    }
}
