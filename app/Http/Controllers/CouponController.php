<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

use App\Coupon;

class CouponController extends Controller
{
    public function index() {
        $coupons = Coupon::all();
        return response()->json(['counted' => $coupons->count(), 'data' => $coupons], 200);
    }
    
    public function suggested() {
        $coupons = Coupon::inRandomOrder()->select('coupons.*', 'clients.client_name', 'clients.client_logo')->join('clients', 'coupons.client_id', "=", "clients.id")->limit(25)->get();
        return response()->json(['count' => $coupons->count(), 'data' => $coupons], 200);
    }
    
    public function show($id) {
        $coupon = Coupon::select('coupons.*', 'clients.client_name', 'clients.client_logo')->join('clients', 'coupons.client_id', '=', 'clients.id')->find($id);
        return response()->json(['data' => $coupon], 200);
    }
    
    public function create() {
        
    }
    
    public function update() {
        
    }
    
    public function destroy() {
        
    }
}
