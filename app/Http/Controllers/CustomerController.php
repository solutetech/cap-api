<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Customer;

class CustomerController extends Controller
{
    public function index() {
        $customer = Customer::all();
        return response()->json(['count'=>$customer->count(), 'data' => $customer], 200);
    }
    
    public function show() {
        
    }
    
    public function create() {
        
    }
    
    public function update() {
        
    }
    
    public function destroy() {
        
    }
}
