<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

use App\Customer;
use App\Coupon;
use App\Used;
use App\Following;

class CustomerCouponsController extends Controller
{
    public function index($id) {
        $customer = Customer::find($id);
        
        if ($customer) {
            $following = Following::select('clients.id as client_id', 'clients.client_name', 'clients.client_logo')->where('customer_id', $customer->id)->join('clients', 'following.client_id', "=", 'clients.id')->get();
            $used = Used::where([['customer_id', $customer->id],['status', 'used']])->join('coupons','used.coupon_id', '=', 'coupons.id')->get();
            $saved = Used::where([['customer_id', $customer->id],['status', 'saved']])->join('coupons','used.coupon_id', '=', 'coupons.id')->get();
            
            $data['customer'] = $customer;
            $data['customer']['follow'] = array('following_count' => $following->count(), 'following' => $following);
            $data['customer']['used'] = array('used_count' => $used->count(), 'coupons' => $used);
            $data['customer']['saved'] = array('saved_count' => $saved->count(), 'coupons' => $saved);
            
            $status = 'success';
            $message = 'Customer found!';
        } else {
            $status = 'failed';
            $message = 'Customer does not exist!';
            $data = "";
        }
        
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], 200);
    }
}