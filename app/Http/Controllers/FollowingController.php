<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Following;

class FollowingController extends Controller
{
    public function index() {
        $following = Following::all();
        return response()->json(['count'=>$following->count(), 'data' => $following], 200);
    }
    
    public function show() {
        
    }
    
    public function create() {
        
    }
    
    public function update() {
        
    }
    
    public function destroy() {
        
    }
}
