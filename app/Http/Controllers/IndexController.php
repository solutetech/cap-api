<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class IndexController extends Controller
{
    // Handle index request
    public function index() {
        $result['message'] = "Welcome to CAP";
        $result['status'] = "Success";
        return response()->json($result, 200);
    }
}
