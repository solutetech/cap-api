<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Used;

class UsedController extends Controller
{
    public function index() {
        $used = Used::all();
        return response()->json(['count'=>$used->count(), 'data' => $used], 200);
    }
    
    public function show() {
        
    }
    
    public function create() {
        
    }
    
    public function update() {
        
    }
    
    public function destroy() {
        
    }
}
