<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateClickRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'coupon_id' => 'required',
            'customer_id' => 'required'
        ];
    }
    
    public function response(array $errors) {
        return response()->json(['message' => 'Need coupon_id and customer_id', 'code' => 422, 422]);
    }
}
