<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'IndexController@index');
Route::resource('clients', 'ClientController', ['except' => ['store','edit']]);
Route::resource('customers', 'CustomerController', ['except' => ['store','edit']]);
Route::resource('campaigns', 'CampaignController', ['except' => ['store','edit']]);
Route::resource('coupons', 'CouponController', ['except' => ['store','edit']]);
Route::resource('cards', 'CardController', ['except' => ['store','edit']]);
Route::resource('clicks', 'ClickController', ['except' => ['edit']]);
Route::resource('following', 'FollowingController', ['except' => ['store','edit']]);
Route::resource('used', 'UsedController', ['except' => ['store','edit']]);

// 
Route::get('client_campaigns/{id}', 'ClientCampaignController@index');

// 
Route::get('customer/{id}', 'CustomerCouponsController@index');


// Get Suggested Coupons
Route::get('recommended', 'CouponController@suggested');