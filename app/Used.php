<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Used extends Model
{
    protected $table = 'used';
    
    protected $fillable = ['coupon_id','customer_id','client_id','status'];
    
    protected $hidden = ['id','created_at','updated_at'];
    
    public function coupon() {
        return $this->belongsTo('App\Coupon');
    }
    
    public function client() {
        return $this->belongsTo('App\Client');
    }
    
    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
