<?php

use Illuminate\Database\Seeder;

use App\Campaign;

use Faker\Factory as Faker;

class CampaignSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        for($i=0; $i<500; $i++) {
            Campaign::create([
                'client_id' => $faker->numberbetween(1,100),
                'campaign_name' => $faker->word,
                'campaign_description' => $faker->sentence,
                'campaign_start_date' => $faker->dateTime,
                'campaign_expiration_date' => $faker->dateTime
            ]);
        }
    }
}
