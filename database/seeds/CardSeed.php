<?php

use Illuminate\Database\Seeder;

use App\Card;

use Faker\Factory as Faker;

class CardSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        for($i=1; $i<=100; $i++) {
            Card::create([
                'client_id' => $i,
                'card_type' => $faker->creditCardType,
                'card_number' => $faker->creditCardNumber,
                'card_expiration' => $faker->creditCardExpirationDate,
                'security_code' => $faker->randomNumber(4),
                'name_on_card' => $faker->name
            ]);
        }
    }
}

