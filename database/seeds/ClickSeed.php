<?php

use Illuminate\Database\Seeder;

use App\Click;

use Faker\Factory as Faker;

class ClickSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        for($i=0; $i<5000; $i++) {
            Click::create([
                'coupon_id' => $faker->numberbetween(1,1000),
                'customer_id' => $faker->numberbetween(1,2000)
            ]);
        }
    }
}
