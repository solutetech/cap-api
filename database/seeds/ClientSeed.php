<?php

use Illuminate\Database\Seeder;

use App\Client;

use Faker\Factory as Faker;

class ClientSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        for($i=0; $i<100; $i++) {
            Client::create([
                'client_name' => $faker->company,
                'client_address' => $faker->streetAddress,
                'client_city' => $faker->city,
                'client_state' => $faker->state,
                'client_zip' => $faker->postcode,
                'client_phone' => $faker->phoneNumber,
                'client_email' => $faker->email,
                'client_password' => $faker->password,
                'client_logo' => $faker->imageUrl($width = 640, $height = 480),
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
            ]);
        }
    }
}