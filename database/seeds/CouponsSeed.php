<?php

use Illuminate\Database\Seeder;

use App\Coupon;

use Faker\Factory as Faker;

class CouponsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        for($i=0; $i<1000; $i++) {
            Coupon::create([
                'campaign_id' => $faker->numberbetween(1,500),
                'client_id' => $faker->numberbetween(1,100),
                'coupon_name' => $faker->word,
                'coupon_description' => $faker->sentence,
                'coupon_image' => $faker->imageUrl($width = 640, $height = 480),
                'coupon_barcode' => $faker->isbn13,
                'coupon_expiration' => $faker->dateTime,
                'used_count' => $faker->randomNumber
            ]);
        }
    }
}
