<?php

use Illuminate\Database\Seeder;

use App\Customer;

use Faker\Factory as Faker;

class CustomerSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        for($i=0; $i<2000; $i++) {
            Customer::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'zip' => $faker->randomNumber(5),
                'email' => $faker->email,
                'dob' => $faker->dateTime(),
                'password' => $faker->password,
                'photo' => $faker->imageUrl,
                'points' => $faker->randomNumber
            ]);
        }
    }
}
