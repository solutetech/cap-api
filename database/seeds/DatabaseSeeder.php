<?php

use Illuminate\Database\Seeder;

use App\Client;
use App\Customer;
use App\Campaign;
use App\Coupon;
use App\Card;
use App\Click;
use App\Following;
use App\Used;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Client::truncate();
        Customer::truncate();
        Campaign::truncate();
        Coupon::truncate();
        Card::truncate();
        Click::truncate();
        Following::truncate();
        Used::truncate();
        
        $this->call('ClientSeed');
        $this->call('CustomerSeed');
        $this->call('CampaignSeed');
        $this->call('CouponsSeed');
        $this->call('CardSeed');
        $this->call('ClickSeed');
        $this->call('FollowingSeed');
        $this->call('UsedSeed');
    }
}
