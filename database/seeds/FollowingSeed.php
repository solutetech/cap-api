<?php

use Illuminate\Database\Seeder;

use App\Following;

use Faker\Factory as Faker;

class FollowingSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        for($i=0; $i<6000; $i++) {
            Following::create([
                'customer_id' => $faker->numberbetween(1,2000),
                'client_id' => $faker->numberbetween(1,100),
            ]);
        }
    }
}
