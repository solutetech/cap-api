<?php

use Illuminate\Database\Seeder;

use App\Used;

use Faker\Factory as Faker;

class UsedSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        for($i=0; $i<4000; $i++) {
            Used::create([
                'coupon_id' => $faker->numberbetween(1,1000),
                'customer_id' => $faker->numberbetween(1,2000),
                'client_id' => $faker->numberbetween(1,100),
                'status' => $faker->randomElement(array('used', 'saved'))
            ]);
        }
    }
}
